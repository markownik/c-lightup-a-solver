// ==UserScript==
// @name         LightUpHelperScript
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @description  try to take over the world!
// @author       You
// @match        http://www.puzzle-light-up.com/*
// @grant        unsafeWindow
// @require      http://code.jquery.com/jquery-latest.min.js
// ==/UserScript==

function getgamearray() {
    var table = $("#LightUpTable");
    var result = [];
    table.find('tr').each(function(){
        var row = [];
        $(this).find('td').each(function(){
            if(!$(this).hasClass('t'))
            {
                row.push('');
            }
            else
            {
                var src = $(this).find('img').attr('src');
                src = src.replace('li','');
                src = src.replace('.gif','');
                src = src.replace('00','b');
                row.push(src);
            }
        });
        result.push(row);
    });
    return result;
}

function setgamearray(input)
{
    wall = new Array();
    for (i = 0; i < 7; i++){
        wall[i] = new Array();
        bulbs[i] = new Array();
        lights[i] = new Array();
        for (j = 0; j < 7; j++){
            wall[i][j] = 0;
            bulbs[i][j] = 0;
            lights[i][j] = 0;
        }
    }

    var table = $("#LightUpTable");
    var row = 0;
    var ans = "";
    table.find('tr').each(function(){
        var column = 0;
        var txt = "";
        $(this).find('td').each(function(){
            $(this).removeClass();
            $(this).find('img').remove();
            //
            var c = '';
            var path = '';
            var name = '';
            //
            if($.inArray(input[row][column],["b","0","1","2","3","4"]) != -1)
            {
                $(this).addClass("t");
                path = "li";
                if(input[row][column] === "b")
                    path += "00";
                else
                    path += input[row][column];
                wall[row][column] = 1;
                ans += "n";
            }
            else
            {
                c = "l";
                path = "nd";
                name = "l_"+row+"_"+column;
                //
                if(input[row][column] == "*")
                {
                    bulbs[row][column] = 1;
                    path = "yl";
                    ans += "y";
                }
                else
                {
                    ans += "n";
                }
            }
            path += '.gif';
            //
            $img = $('<img>', {class: c, name: name, src: path});
            $(this).append($img);
            ++column;
        });
        ++row;
    });
    init();
    return ans;
}

function savetofile() {
    var table = getgamearray();
    var properties = {param: $("form[name='answerForm'] input[name='param']").val(),
                 w: $("form[name='answerForm'] input[name='w']").val(),
                 h: $("form[name='answerForm'] input[name='h']").val()
                };
    var result = {table, properties};
    var file = new Blob([JSON.stringify(result)], {type: 'text/plain'});
    if($('a#backup').length)
    {
        $('a#backup').attr({href: URL.createObjectURL(file), download: 'savegame.json'});
    }
    else
    {
        $a = $('<a>', {id: "backup", href: URL.createObjectURL(file), download: 'savegame.json'});
        $('#MainContainer').append($a);
    }
    $('a#backup')[0].click();
}

function loadfromfile()
{
    $("#file").change(function(evt){
        var file = evt.target.files[0];
        try {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    console.log(e);
                    try{
                        var data = JSON.parse(e.target.result);
                        var params = data.properties;
                        cw = $("form[name='answerForm'] input[name='w']").val();
                        if(params.w != cw)
                        {
                            throw 'Wrong field size; is '+cw+', should be '+params.w+'!';
                        }
                        $("form[name='answerForm'] input[name='w']").val(params.w);
                        $("form[name='answerForm'] input[name='w']").val(params.h);
                        $("form[name='answerForm'] input[name='param']").val(params.param);
                        var ansh = setgamearray(data.table);
                        $("form[name='answerForm'] input[name='ansH']").val(ansh);
                    }
                    catch (ex)
                    {
                        alert(ex);
                    }
                };
            })(file);
            reader.readAsText(file);
        }
        catch (ex)
        {
            alert(ex);
        }
    });
    $("#file").click();
}

$(document).ready(function(){
    var scr = document.createElement('script');
    scr.type = "text/javascript";
    scr.src = "http://code.jquery.com/jquery-latest.min.js";
    document.getElementsByTagName('head')[0].appendChild(scr);
    
    $input = $('<input>', {class: 'button', type: 'button', name:'save', onclick: 'savetofile(); return false;', value: '   Save to JSON   '});
    $input.insertAfter($("form[name='answerForm'] input[name='new']"));
    $input = $('<input>', {class: 'button', type: 'button', name:'load', onclick: 'loadfromfile(); return false;', value: '   Load from JSON   '});
    $input.insertAfter($("form[name='answerForm'] input[name='save']"));
    $input = $('<input>', {class: '', type: 'file', id:'file', name:'not-important', style:'position:absolute; top:0; left:0; width:1px; height:1px; opacity:0;'});
    $input.insertAfter($("form[name='answerForm'] input[name='load']"));
    //styling
    $("form[name='answerForm'] input[name='new']").parent().css({'display':'table','margin':'0 auto'});
    $("form[name='answerForm'] .button").css({'margin':'2px','padding':'3px','display':'table-cell'});
    
    scr = document.createElement('script');
    scr.type="text/javascript";
    scr.textContent = savetofile.toString() + "\n" + loadfromfile.toString() + "\n" + getgamearray.toString() + "\n" + setgamearray.toString();
    document.getElementsByTagName('head')[0].appendChild(scr);
});