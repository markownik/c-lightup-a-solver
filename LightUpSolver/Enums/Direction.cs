﻿namespace LightUpSolver.Enums
{
    public enum Direction
    {
        Left = 0,
        Up = 1,
        Right = 2,
        Down = 3
    }

    public static class DirectionExtensions
    {
        public static int Switch(this Direction dir)
        {
            return ((int)dir - 2 > -1) ? (int)dir - 2 : (int)dir + 2;
        }
    }
}
