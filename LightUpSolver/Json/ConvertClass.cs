﻿using Newtonsoft.Json;

namespace LightUpSolver.Json
{
    public class ConvertClass
    {
        public class Params
        {
            [JsonProperty("param")]
            public string Param { get; set; }

            [JsonProperty("w")]
            public int Width { get; set; }

            [JsonProperty("h")]
            public int Height { get; set; }
        }

        [JsonProperty("table")]
        public string[][] Table { get; set; }

        [JsonProperty("properties")]
        public Params Properties { get; set; }
    }
}
