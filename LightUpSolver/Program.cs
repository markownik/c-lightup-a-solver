﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using LightUpSolver.Json;
using LightUpSolver.Logic;
using LightUpSolver.AStar;
using System.Text;
using System.Diagnostics;

namespace LightUpSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() > 0)
            {
                try
                {
                    ConvertClass result;

                    using (StreamReader sr = new StreamReader(args[0]))
                    {
                        result = JsonConvert.DeserializeObject<ConvertClass>(sr.ReadToEnd());
                        sr.Close();
                    }

                    Stopwatch stopwatch = Stopwatch.StartNew();
                    Solver AStarSolver = new Solver(new GameState(result.Table));
                    Node final = AStarSolver.Solve();
                    stopwatch.Stop();

                    List<Node> solution = AStarSolver.Solution;

                    var log = args[0].Split('.');
                    log[log.Count() - 2] += "_log";
                    log[log.Count() - 1] = "txt";

                    using (StreamWriter swl = new StreamWriter(String.Join(".", log)))
                    {
                        int i = 0;
                        foreach (Node n in solution)
                        {
                            var buff = new StringBuilder();
                            if (i == 0) { buff.Append("Initial"); }
                            if (i > 0 && i < solution.Count() - 1) { buff.Append("Step " + i.ToString() + " "); }
                            if (i == solution.Count() - 1) { buff.Append("Final "); }
                            buff.Append(n.Game.ToString());
                            Console.WriteLine(buff.ToString());
                            swl.WriteLine(buff.ToString());
                            ++i;
                        }
                        Console.WriteLine("Solving took " + (float)stopwatch.ElapsedMilliseconds / 1000 + " seconds.\n");
                        swl.WriteLine("Solving took " + (float)stopwatch.ElapsedMilliseconds / 1000 + " seconds.\n");
                    }

                    var save = args[0].Split('.');
                    save[save.Count() - 2] += "_solution";
                    result.Table = final.Game.ToArray();

                    using (StreamWriter sw = new StreamWriter(String.Join(".", save)))
                    {
                        sw.Write(JsonConvert.SerializeObject(result));
                        sw.Close();
                    }
                }
                catch (JsonException e)
                {
                    Console.WriteLine("There was error when parsing JSON:");
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                }
            }
            Console.ReadLine();
        }
    }
}
