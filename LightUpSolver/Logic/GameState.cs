﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace LightUpSolver.Logic
{
    class GameState
    {
        private Field[][] _table;
        private int _width;
        private int _height;
        private List<Coord> _unlit;
        private List<Coord> _lit;
        private List<Coord> _bulbs;
        private List<Coord> _black;
        private Coord _lightup;

        public List<Coord> Unlit { get => _unlit; }
        public List<Coord> Lit { get => _lit; }
        public List<Coord> Bulbs { get => _bulbs; }
        public List<Coord> Black { get => _black; }
        public int Need { get => _black.Sum(item => GetFieldXY(item).Need); }
        public int Height { get => _height; }
        public int Width { get => _width; }
        public Coord LastLight { get => _lightup; }

        private void Update()
        {
            _unlit = new List<Coord>();
            _lit = new List<Coord>();
            _bulbs = new List<Coord>();

            for (int y = 0; y < _height; ++y)
            {
                for (int x = 0; x < _width; ++x)
                {
                    Field current = _table[y][x];
                    if(current.Type == Field.FType.Normal)
                    {
                        switch(current.State)
                        {
                            case Field.FState.NotLit:
                                _unlit.Add(current.Position);
                                break;
                            case Field.FState.Lit:
                                _lit.Add(current.Position);
                                break;
                        }
                    }
                }
            }
        }

        public Field GetFieldXY(Coord pos)
        {
            if (pos.x >= 0 && pos.x < _width && pos.y >= 0 && pos.y < _height)
                return _table[pos.y][pos.x];
            else
                return null;
        }

        public GameState()
        {
            _table = null;
            _width = 0;
            _height = 0;
            _unlit = null;
            _lit = null;
            _bulbs = null;
            _black = null;
        }

        public GameState(GameState old)
        {
            _height = old._height;
            _width = old._width;
            _table = new Field[_height][];
            _unlit = new List<Coord>(old._unlit);
            _lit = new List<Coord>(old._lit);
            _bulbs = new List<Coord>(old._bulbs);
            _black = new List<Coord>(old._black);
            _lightup = new Coord(old._lightup);

            for (int y = 0; y < _height; ++y)
            {
                _table[y] = new Field[_width];
                for (int x = 0; x < _width; ++x)
                {
                    _table[y][x] = new Field(old._table[y][x], this);
                }
            }
        }

        public GameState(string[][] array)
        {
            _height = array.Count();
            _width = array[0].Count();
            _table = new Field[_height][];
            _unlit = new List<Coord>();
            _lit = new List<Coord>();
            _bulbs = new List<Coord>();
            _black = new List<Coord>();
            _lightup = new Coord(-1, -1);

            for (int y = 0; y < _height; ++y)
            {
                _table[y] = new Field[_width];
                for (int x = 0; x < _width; ++x)
                {
                    _table[y][x] = new Field(array[y][x], this);
                    _table[y][x].Position = new Coord(x,y);
                    if (_table[y][x].Need > -1)
                    {
                        _black.Add(new Coord(x, y));
                    }
                    if(_table[y][x].Type == Field.FType.Normal)
                    {
                        _unlit.Add(new Coord(x, y));
                    }
                }
            }
        }

        public bool LightUpXY(Coord pos)
        {
            if(_table[pos.y][pos.x].LightUp())
            {
                _lightup = new Coord(pos);
                _bulbs.Add(pos);
                Update();
                return true;
            }
            else
            {
                return false;
            }
        }

        public string[][] ToArray()
        {
            var array = new string[_height][];
            for (int y = 0; y < _height; ++y)
            {
                array[y] = new string[_width];
                for (int x = 0; x < _width; ++x)
                {
                    array[y][x] = _table[y][x].ToString();
                    if (array[y][x] == ".") { array[y][x] = ""; }
                    if (array[y][x] == "#") { array[y][x] = "b"; }
                }
            }
            return array;
        }

        public override string ToString()
        {
            StringBuilder ret = new StringBuilder();
            ret.Append("Gamestate:\n");
            ret.Append("\n\t|");
            foreach (var column in _table[0])
            { 
                ret.Append("----|");
            }
            ret.Append("\n");
            foreach (var row in _table)
            {
                StringBuilder span = new StringBuilder();
                ret.Append("\t|  ");
                span.Append("\t|");
                foreach(Field field in row)
                {
                    ret.Append(field.ToString());
                    ret.Append(" |  ");
                    span.Append("----|");
                }
                ret.Append("\n");
                span.Append("\n");
                ret.Append(span.ToString());
            }
            return ret.ToString();
        }

    }
}
