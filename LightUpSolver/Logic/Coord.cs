﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightUpSolver.Logic
{
    public class Coord : IEquatable<Coord>, IComparable<Coord>
    {
        public int x { get; set; }
        public int y { get; set; }

        public Coord()
        {
            x = y = 0;
        }

        public Coord(int ax, int ay)
        {
            x = ax;
            y = ay;
        }

        public Coord(Coord old)
        {
            x = old.x;
            y = old.y;
        }

        public override string ToString()
        {
            return new StringBuilder().Append(x.ToString()).Append(", ").Append(y.ToString()).ToString();
        }

        public bool Equals(Coord other)
        {
            return (x == other.x && y == other.y);
        }

        public int CompareTo(Coord other)
        {
            if (other == null)
                return 1;

            if (this == other)
                return 0;

            if (y < other.y)
                return -1;

            if (y == other.y)
            {
                if (x < other.x)
                    return -1;

                if (x == other.x)
                    return 0;

                if (x > other.x)
                    return 1;
            }

            if (y > other.y)
                return 1;

            return 0;
        }
    }
}
