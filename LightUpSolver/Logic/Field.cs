﻿using System.Linq;
using System.Collections.Generic;
using LightUpSolver.Enums;
using System;

namespace LightUpSolver.Logic
{
    partial class Field
    {
        private FType _type;
        private int _need;
        private int _have;
        private List<Coord> _neighbors;
        private Coord _position;
        private GameState _root;

        public FType Type { get => _type; }
        public FState State { get; set; }
        public List<Coord> Neighbors { get => _neighbors; set => _neighbors = value; }
        public int Need { get => _need - _have; }
        public Coord Position { get => _position; set => SetPosition(value); }

        public Field(Field old, GameState root)
        {
            _type = old._type;
            _need = old._need;
            _have = old._have;
            State = old.State;
            Position = new Coord(old.Position);
            _neighbors = new List<Coord>(old.Neighbors);
            _root = root;
        }

        public Field(string sign, GameState root)
        {
            _type = FType.Normal;
            _need = -1;
            _have = 0;
            State = FState.NotLit;
            Position = new Coord();
            _root = root;
            if (sign == "b")
            {
                _type = FType.Black;
                return;
            }
            if(int.TryParse(sign, out int t))
            {
                _type = FType.Black;
                _need = int.Parse(sign);
                _have = 0;
                return;
            }
        }

        private void SetPosition(Coord pos)
        {
            _position = pos;
            _neighbors = new List<Coord> {
                new Coord(pos.x - 1, pos.y),
                new Coord(pos.x, pos.y - 1),
                new Coord(pos.x + 1, pos.y),
                new Coord(pos.x, pos.y + 1)
            };
        }

        public bool LightUp()
        {
            if (CanLightUp())
            {
                State = FState.Lighbulb;
                for(int i = 0; i < 4; ++i)
                {
                    var n = _root.GetFieldXY(_neighbors[i]);
                    if (n != null)
                    {
                        n.Light((Direction)i);
                    }
                }
                return true;
            }
            return false;
        }
        
        public bool CanLightUp()
        {
            if (Type == FType.Normal && State == FState.NotLit)
            {
                for (int i = 0; i < 4; ++i)
                {
                    var n = _root.GetFieldXY(_neighbors[i]);
                    if (n != null)
                    {
                        if (n.Need == 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            else
                return false;
        }

        public void Light(Direction dir)
        {
            if (_type == FType.Normal)
            {
                State = FState.Lit;
                var n = _root.GetFieldXY(_neighbors[(int)dir]);
                if (n != null)
                {
                    n.Light(dir);
                }
            }
            else
            {
                var n = _root.GetFieldXY(_neighbors[(int)dir.Switch()]);
                if (n.State == FState.Lighbulb)
                {
                    if (_need > -1)
                    {
                        ++_have;
                    }
                }
            }
        }

        public int CanHave()
        {
            int c = 0;
            for (int i = 0; i < 4; ++i)
            {
                var n = _root.GetFieldXY(_neighbors[i]);
                if (n != null)
                {
                    if (n.CanLightUp()) { c++; }
                }
            }
            return c;
        }

        public override string ToString()
        {
            if (_type == FType.Black)
            {
                if (_need > -1)
                    return _need.ToString();
                else
                    return "#";
            }
            else
                return State.ToFriendlyString();
        }
    }
}
