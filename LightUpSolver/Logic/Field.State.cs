﻿namespace LightUpSolver.Logic
{
    partial class Field
    {
        public enum FState
        {
            NotLit,
            Lit,
            Lighbulb
        }
    }

    static class FStateExtensions
    {
        public static string ToFriendlyString(this Field.FState state)
        {
            switch (state)
            {
                case Field.FState.Lighbulb:
                    return "*";
                case Field.FState.Lit:
                    return ".";
                default:
                    return " ";
            }
        }
    }
}
