﻿using LightUpSolver.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightUpSolver.AStar
{
    class Node : IComparable<Node>
    {
        private List<Node> _children;
        private Node _root;
        private GameState _game;
        private int _total;
        private int _cost = 1;

        public Node Root { get => _root; }
        public GameState Game { get => _game; }
        public List<Node> Children { get => _children; }
        public int Total { get => _total; set => _total = value; }
        public int Cost { get => _cost; }
        public int Heuristic { get => _game.Unlit.Count() + (_game.Need * 1000); }

        public Node(GameState game)
        {
            _root = null;
            _game = game;
            _total = 0;
            _cost = int.MaxValue;
        }

        public Node(Node parent, GameState game)
        {
            _root = parent;
            _game = game;
            _total = parent._total + _cost;
        }

        public List<Node> GenerateChildren()
        {
            _children = new List<Node>();
            int curr_count = _game.Bulbs.Count();
            foreach (var field in _game.Unlit)
            {
                GameState game = new GameState(_game);

                if (field.CompareTo(game.LastLight) > 0)
                {
                    if (game.LightUpXY(field))
                    {
                        _children.Add(new Node(this, game));
                    }
                }
            }
            return _children;
        }

        public int CompareTo(Node other)
        {
            if (other == null)
                return 1;
            else
                return (Total + Heuristic).CompareTo(other.Total + other.Heuristic);
        }
    }
}
