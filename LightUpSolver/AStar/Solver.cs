﻿using LightUpSolver.Logic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LightUpSolver.AStar
{
    class Solver
    {
        private List<Node> _search;
        private List<Node> _visited;
        private List<Node> _solution_path;

        public List<Node> Solution { get => _solution_path; }
        
        private void AddToSearch(Node n)
        {
            _search.Add(n);
        }

        private void AddToSearch(List<Node> nl)
        {
            _search = _search.Concat(nl).ToList();
        }

        public Solver()
        {
            _search = new List<Node>();
            _visited = new List<Node>();
        }

        public Solver(GameState starting)
        {
            _search = new List<Node>();
            _visited = new List<Node>();
            _search.Add(new Node(starting));
        }

        public Node Solve()
        {
            Node current = _search[0];
            while (_search.Count() > 0)
            {
                if(current.Game.Unlit.Count() == 0 && current.Game.Need == 0)
                {
                    Finish(current);
                    return current;
                }

                AddToSearch(current.GenerateChildren());

                _visited.Add(current);
                _search.Remove(current);

                //Console.WriteLine(current.Game.ToString());

                int lowest = int.MaxValue;
                foreach (var s in _search)
                {
                    if (s.Total + s.Heuristic < lowest)
                    {
                        lowest = s.Total + s.Heuristic;
                        current = s;
                    }
                }
            }
            throw new Exception("Solver Error - Result Not Found");
        }

        public void Solve(GameState starting)
        {
            _search = new List<Node>();
            _visited = new List<Node>();
            _search.Add(new Node(starting));
            try
            {
                Solve();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error during solving:");
                Console.WriteLine(e.Message);
            }
        }

        public void Finish(Node solved)
        {
            _solution_path = new List<Node>();
            Node current = solved;
            do {
                _solution_path.Add(current);
                current = current.Root;
            } while (current != null);
            _solution_path.Reverse();
        }
    }
}
