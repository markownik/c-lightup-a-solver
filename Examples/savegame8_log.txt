InitialGamestate:

	|----|----|----|----|----|----|----|
	|    |    |    |    |  2 |    |    |  
	|----|----|----|----|----|----|----|
	|    |    |    |  2 |    |    |    |  
	|----|----|----|----|----|----|----|
	|  1 |    |    |    |    |    |    |  
	|----|----|----|----|----|----|----|
	|    |  0 |    |    |    |  0 |    |  
	|----|----|----|----|----|----|----|
	|    |    |    |    |    |    |  # |  
	|----|----|----|----|----|----|----|
	|    |    |    |  # |    |    |    |  
	|----|----|----|----|----|----|----|
	|    |    |  3 |    |    |    |    |  
	|----|----|----|----|----|----|----|

Step 1 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |    |    |  
	|----|----|----|----|----|----|----|
	|    |    |    |  2 |    |    |    |  
	|----|----|----|----|----|----|----|
	|  1 |    |    |    |    |    |    |  
	|----|----|----|----|----|----|----|
	|    |  0 |    |    |    |  0 |    |  
	|----|----|----|----|----|----|----|
	|    |    |    |    |    |    |  # |  
	|----|----|----|----|----|----|----|
	|    |    |    |  # |    |    |    |  
	|----|----|----|----|----|----|----|
	|    |    |  3 |    |    |    |    |  
	|----|----|----|----|----|----|----|

Step 2 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|    |    |    |  2 |    |  . |    |  
	|----|----|----|----|----|----|----|
	|  1 |    |    |    |    |  . |    |  
	|----|----|----|----|----|----|----|
	|    |  0 |    |    |    |  0 |    |  
	|----|----|----|----|----|----|----|
	|    |    |    |    |    |    |  # |  
	|----|----|----|----|----|----|----|
	|    |    |    |  # |    |    |    |  
	|----|----|----|----|----|----|----|
	|    |    |  3 |    |    |    |    |  
	|----|----|----|----|----|----|----|

Step 3 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  2 |    |  . |    |  
	|----|----|----|----|----|----|----|
	|  1 |    |    |    |    |  . |    |  
	|----|----|----|----|----|----|----|
	|    |  0 |    |    |    |  0 |    |  
	|----|----|----|----|----|----|----|
	|    |    |    |    |    |    |  # |  
	|----|----|----|----|----|----|----|
	|    |    |    |  # |    |    |    |  
	|----|----|----|----|----|----|----|
	|    |    |  3 |    |    |    |    |  
	|----|----|----|----|----|----|----|

Step 4 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  2 |  . |  . |  * |  
	|----|----|----|----|----|----|----|
	|  1 |    |    |    |    |  . |  . |  
	|----|----|----|----|----|----|----|
	|    |  0 |    |    |    |  0 |  . |  
	|----|----|----|----|----|----|----|
	|    |    |    |    |    |    |  # |  
	|----|----|----|----|----|----|----|
	|    |    |    |  # |    |    |    |  
	|----|----|----|----|----|----|----|
	|    |    |  3 |    |    |    |    |  
	|----|----|----|----|----|----|----|

Step 5 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  2 |  . |  . |  * |  
	|----|----|----|----|----|----|----|
	|  1 |  . |  . |  * |  . |  . |  . |  
	|----|----|----|----|----|----|----|
	|    |  0 |    |  . |    |  0 |  . |  
	|----|----|----|----|----|----|----|
	|    |    |    |  . |    |    |  # |  
	|----|----|----|----|----|----|----|
	|    |    |    |  # |    |    |    |  
	|----|----|----|----|----|----|----|
	|    |    |  3 |    |    |    |    |  
	|----|----|----|----|----|----|----|

Step 6 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  2 |  . |  . |  * |  
	|----|----|----|----|----|----|----|
	|  1 |  . |  . |  * |  . |  . |  . |  
	|----|----|----|----|----|----|----|
	|  . |  0 |    |  . |    |  0 |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  . |  . |  . |  # |  
	|----|----|----|----|----|----|----|
	|  . |    |    |  # |    |    |    |  
	|----|----|----|----|----|----|----|
	|  . |    |  3 |    |    |    |    |  
	|----|----|----|----|----|----|----|

Step 7 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  2 |  . |  . |  * |  
	|----|----|----|----|----|----|----|
	|  1 |  . |  . |  * |  . |  . |  . |  
	|----|----|----|----|----|----|----|
	|  . |  0 |  . |  . |    |  0 |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  . |  . |  . |  # |  
	|----|----|----|----|----|----|----|
	|  . |  . |  * |  # |    |    |    |  
	|----|----|----|----|----|----|----|
	|  . |    |  3 |    |    |    |    |  
	|----|----|----|----|----|----|----|

Step 8 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  2 |  . |  . |  * |  
	|----|----|----|----|----|----|----|
	|  1 |  . |  . |  * |  . |  . |  . |  
	|----|----|----|----|----|----|----|
	|  . |  0 |  . |  . |  . |  0 |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  . |  . |  . |  # |  
	|----|----|----|----|----|----|----|
	|  . |  . |  * |  # |  * |  . |  . |  
	|----|----|----|----|----|----|----|
	|  . |    |  3 |    |  . |    |    |  
	|----|----|----|----|----|----|----|

Step 9 Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  2 |  . |  . |  * |  
	|----|----|----|----|----|----|----|
	|  1 |  . |  . |  * |  . |  . |  . |  
	|----|----|----|----|----|----|----|
	|  . |  0 |  . |  . |  . |  0 |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  . |  . |  . |  # |  
	|----|----|----|----|----|----|----|
	|  . |  . |  * |  # |  * |  . |  . |  
	|----|----|----|----|----|----|----|
	|  . |  * |  3 |    |  . |    |    |  
	|----|----|----|----|----|----|----|

Final Gamestate:

	|----|----|----|----|----|----|----|
	|  . |  . |  . |  * |  2 |  * |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  2 |  . |  . |  * |  
	|----|----|----|----|----|----|----|
	|  1 |  . |  . |  * |  . |  . |  . |  
	|----|----|----|----|----|----|----|
	|  . |  0 |  . |  . |  . |  0 |  . |  
	|----|----|----|----|----|----|----|
	|  * |  . |  . |  . |  . |  . |  # |  
	|----|----|----|----|----|----|----|
	|  . |  . |  * |  # |  * |  . |  . |  
	|----|----|----|----|----|----|----|
	|  . |  * |  3 |  * |  . |  . |  . |  
	|----|----|----|----|----|----|----|

Solving took 3,573 seconds.

